# -*- coding: utf8 -*-

import os


def get_version():
    folder, _ = os.path.split(os.path.realpath(__file__))
    pack_folder, _ = os.path.split(folder)
    messages_json = os.path.join(pack_folder, 'messages.json')
    with open(messages_json, 'r') as message_file:
        message_data = message_file.read()

    ver = message_data.splitlines()[-2].split(':')[0].strip().replace('"', '')
    version = tuple([int(i) for i in ver.split('.')])
    return version
