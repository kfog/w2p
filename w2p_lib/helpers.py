# -*- coding: utf-8 -*-

import os
import sublime
from collections import namedtuple


def get_settings():
    """Read the default plugin settings and return it"""

    plugin_settings = sublime.load_settings('w2p.sublime-settings')
    return plugin_settings


def get_application_preferences():
    """Create a namedtuple with web2py application data"""

    application = namedtuple('application', [
            'web2py_folder',
            'application_path',
            'applications_folder_path',
            'application_name'])
    project_folder = sublime.active_window().folders()[0]
    tmp, _ = os.path.split(project_folder)
    w2p_folder, _ = os.path.split(tmp)
    app_folder_path, app_name = os.path.split(project_folder)
    return application._make([
        w2p_folder,
        project_folder,
        app_folder_path, app_name])


def validate_project(project_folders):
    """ Check if the folder added to the project contains
    a valid we2py application"""

    if not project_folders:
        return False
    project_folder = project_folders[0]
    app_path, _ = os.path.split(project_folder)
    w2p_folder, _ = os.path.split(app_path)
    if os.path.isfile(os.path.join(w2p_folder, 'web2py.py')):
        return True
    else:
        return False


def get_w2p_server_view():
    """Return the view/buffer associated with web2py server output"""

    for w in sublime.windows():
        for v in w.views():
            if v.name() == '*WEB2PY-SERVER*':
                return v


def split_file(f):
    fileprop = namedtuple(
        'fileprop',
        ['full_file_name', 'file_name', 'ext',
            'folder_name', 'folder_path', 'full_path'])
    _, full_name = os.path.split(f)
    fp, ext = os.path.splitext(f)
    path, fn = os.path.split(fp)
    _, fol_name = os.path.split(path)
    return fileprop(full_name, fn, ext, fol_name, path, f)


def list_controller_files():
    app_prefs = get_application_preferences()
    controller_files = [
        f for f in os.listdir(
            os.path.join(app_prefs.application_path, 'controllers'))
        if filter_python_files(f)]
    return controller_files


def list_model_files():
    app_prefs = get_application_preferences()
    controller_files = [
        f for f in os.listdir(
            os.path.join(app_prefs.application_path, 'models'))
        if filter_python_files(f)]
    return controller_files


def filter_python_files(f):
    """Filter function to show only py file in a folder"""

    fname, fext = os.path.splitext(f)
    if fext == '.py':
        return True
    else:
        return False


def active_view():
    """ Return the currently active view"""

    return sublime.active_window().active_view()
