# -*- coding: utf-8 -*-

import sublime
import sublime_plugin

from .commands import *
from .w2p_lib import helpers
from .listeners import *


def plugin_loaded():
    """Called directly from sublime on plugin load"""
    if not helpers.validate_project(sublime.active_window().folders()):
        sublime.status_message('Please add web2py application to the project')
    else:
        sublime.status_message('Valid web2py application configured')


class ExampleWindowCommand(sublime_plugin.WindowCommand):
    def run(self):
        print('window command')


class ExampleTextCommand(sublime_plugin.TextCommand):

    def run(self, edit):
        print('Edit')
