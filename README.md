# README #


A simple [web2py](http://www.web2py.com) plugin for [Sublime Text 3](http://www.sublimetext.com/3).

I started to develop this plugin because I use both applications on daily basis and I'd like to leave Sublime as little as possible

I hope you will find the plugin as useful as I do


### What is this repository for? ###

* This repository holds the web2py plugin for Sublime Text 3 called W2P
* V 0.3.1


### How do I get set up? ###
1. Through package control
    1. Open Sublime Text
    2. Start package control*
    3. Search W2P and install
    4. Add a web2py application folder to Sublime Text 3 to make the plugin work
    5. Restart Sublime Text
2. Manual
    1. Clone this repo
    2. Copy the content into your Sublime Text 3 Packages directory
        1. Open Sublime Text 3
        2. Go to Preferences menu
        3. Choose Browse Packages
    3. Add a web2py application folder to Sublime Text 3 to make the plugin work
    4. Restart Sublime Text

The plugin is developed on Windows7/Ubuntu 15.04

It is tested on those environtments. If you have a mac and would like to test I would be very appreciated for any feed back


### Features ###

- Start web2py web server
    - Port, password, cert, key can be configured in settings
- Stop web2py web server
- Automatically restart web server if a module file is modified (manual restart also possible)
- Clear web2py application errors (deletes files from application/errors folder)
- Run tests from specific folder in web2py environtment uses (web2py -S {app} -M -R test_file.py)
    - You can specify a test location in settings, or if non provided the following folders will be checked:
        - {app}/tests
        - web2py/tests/{app}
- Setup SublimeREPL integration for web2py {app} if the plugin is installed (Interactive web2py shell from Sublime)
- Open view file associated with the function under the cursor
- Insert models + modules as `from {model/module} import *` after a `if False:` to prevent run but to make Sublime auto completion aware


### Contribution guidelines ###

If you would like to contribute fork the repo and submit a pull request
If you would like to see any feature please open an enhancement ticket with your feature request


### I would like to say thank you to ###

All Sublime Text developrs (Unfortunately I don't know names :()

[Massimo Di Pierro](https://github.com/mdipierro) and all the contributors for web2py

[Oscar Campos (DamnWidget)](https://github.com/DamnWidget) for Anaconda plugin which helped me a lot to write my own

[Will Bond](https://github.com/wbond) for his article on [Tutsplus.com](http://code.tutsplus.com/tutorials/how-to-create-a-sublime-text-2-plugin--net-22685) and Package Control

[Wojciech Bederski](https://github.com/wuub) for SublimeREPL plugin

at last but not least:

[Fred Kornyev](https://bitbucket.org/fkornyev) who gave me time beside my daily tasks at work and also helps with testing the plugin


### Who do I talk to? ###

If you have any question feel free to use the issue tracker or contact me directly via e-mail: gergelyo@gmail.com
