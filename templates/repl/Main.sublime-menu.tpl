[
     {
        "id": "tools",
        "children":
        [{
            "caption": "SublimeREPL",
            "mnemonic": "r",
            "id": "SublimeREPL",
            "children":
            [
                {"command": "repl_open",
                 "caption": "Web2py $app_name",
                 "id": "repl_web2py_$app_name",
                 "mnemonic": "w",
                 "args": {
                    "type": "subprocess",
                    "encoding": "utf8",
                    "cmd": ["python", "-u", $w2p_folder, "-S", "$app_name", "-M", "-P"],
                    "cwd": "$file_path",
                    "external_id": "w2p_$app_name",
                    "extend_env": {"PYTHONIOENCODING": "utf-8"},
                    "syntax": "Packages/Python/Python.tmLanguage"
                    }
                }
            ]
        }]
    }
]
