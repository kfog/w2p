[
    {
        "caption": "SublimeREPL: Web2py $app_name",
        "command": "run_existing_window_command", "args":
        {
            "id": "repl_web2py_$app_name",
            "file": "config/Web2py_$app_name/Main.sublime-menu"
        }
    }
]
