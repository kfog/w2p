# -*- coding: utf-8 -*-

import sublime
import sublime_plugin
from os import path, listdir, remove

from ..w2p_lib.helpers import get_application_preferences


class W2pClearErrors(sublime_plugin.WindowCommand):
    """ Command to clear errors from application/errors folder if any"""

    def run(self):
        app_pref = get_application_preferences()
        errors_folder = path.join(app_pref.application_path, 'errors')
        if path.exists(errors_folder):
            error_count = len(listdir(errors_folder))
            if error_count:
                output_panel = self.window.create_output_panel('op')
                self.window.run_command('show_panel', {'panel': 'output.op'})
                for f in listdir(errors_folder):
                    output_panel.run_command(
                        'append',
                        {'characters': 'Removing file: {df}\n'.format(df=f)})
                    remove(path.join(errors_folder, f))
                output_panel.run_command(
                    'append',
                    {'characters':
                        'Removed {ec} error file(s)'.format(ec=error_count)})
            else:
                sublime.message_dialog('No errors to delete')
        else:
            sublime.message_dialog('No "errors" folder in {app} yet'.format(
                app=app_pref.application_name))
