# -*- coding: utf-8 -*-

import os
import re

import sublime
import sublime_plugin

from ..w2p_lib.helpers import get_application_preferences
from ..w2p_lib.helpers import list_controller_files


class W2pOpenViewFile(sublime_plugin.TextCommand):
    """ Command to open view file associated to the
    given controller function"""

    def run(self, edit):
        # f, ext = os.path.splitext(self.view.file_name())
        # _, cont_name = os.path.split(f)
        controllers = list_controller_files()
        _, controller_file = os.path.split(self.view.file_name())
        ct_name, ext = os.path.splitext(controller_file)
        if controller_file not in controllers:
            sublime.error_message('Invalid controller')
            return
        app_prefs = get_application_preferences()
        p = re.compile(r'(.*response\.view.*)')
        row, col = self.view.rowcol(self.view.sel()[0].begin())
        tp = self.view.text_point(row, 0)
        func_region = self.view.find(r'(?s)(?<=def)(.+?)(?=return)', tp)
        func_text = self.view.substr(func_region)
        # print(func_text)
        res_view = re.search(p, func_text)
        if res_view:
            string = res_view.group(0)
            print(string)
            if '#' not in string:
                _, ft = string.strip().split('=')
                fn_text = ft.strip().strip("'")
            else:
                fn_reg = self.view.find(r'(?<=def\s)(.*)(?=\()', tp)
                fn_text = self.view.substr(fn_reg)
                # print('fn: ', fn_text.strip())
        else:
            fn_reg = self.view.find(r'(?<=def\s)(.*)(?=\()', tp)
            fn_text = self.view.substr(fn_reg)
            # print('fn: ', fn_text.strip())
        # _, controller_file = os.path.split(self.view.file_name())
        # ct_name, _ = os.path.splitext(controller_file)
        # print(ct_name)
        view_path = os.path.join(
            app_prefs.application_path,
            'views',
            ct_name,
            '{fn}.html'.format(fn=fn_text.strip()))
        if not os.path.exists(view_path):
            view_path = os.path.join(
                app_prefs.application_path, 'views', fn_text)
        # print('vp: ', view_path)
        if os.path.exists(view_path):
            self.view.window().open_file(view_path)
        else:
            sublime.error_message('Cannot find view file for this function')

