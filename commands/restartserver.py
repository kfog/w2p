# -*- coding: utf-8 -*-

import sublime
import sublime_plugin

from ..w2p_lib.helpers import get_w2p_server_view


class W2pRestartServer(sublime_plugin.WindowCommand):
    """ Command to restart the development server """

    def run(self):
        v = get_w2p_server_view()
        if v:
            self.window.run_command("w2p_stop_server")
            self.window.run_command("w2p_start_server")
        else:
            self.window.run_command("w2p_start_server")
