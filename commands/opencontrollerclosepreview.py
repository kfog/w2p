# -*- coding: utf-8 -*-

import sublime_plugin


class W2pOpenControllerClosePreview(sublime_plugin.WindowCommand):
    """ Close the open preview pane"""

    def run(self):
        self.window.focus_group(0)
        self.window.run_command('close_pane')
