# -*- coding: utf-8 -*-

import os
import re
import sublime
import sublime_plugin


from ..w2p_lib.helpers import get_application_preferences, split_file, list_controller_files


class W2pOpenController(sublime_plugin.TextCommand):
    """ Command to open the function in the controller the view belongs to"""

    def __init__(self, view):
        sublime_plugin.TextCommand.__init__(self, view)
        self.view = view
        self.app_prefs = get_application_preferences()

    def is_result_view(self, view):
        """ check if the given view is a open controller result view """
        return view.settings().get("open_controller_result_view", False) == True

    def get_result_view(self):
        """ Loop over all views and check returns the opne controller result view """
        active_window = sublime.active_window()
        for view in active_window.views():
            if self.is_result_view(view):
                return view

        result_view = active_window.new_file()
        result_view.set_name('OpenControllerResult')
        result_view.set_scratch(True)
        result_view.settings().set('open_controller_result_view', True)
        result_view.settings().set('command_mode', True)
        result_view.settings().set('word_wrap', False)
        result_view.settings().set("line_numbers", False)
        result_view.assign_syntax(r"Packages/Text/Plain text.tmLanguage")

        return result_view

    def search_controllers(self, controllers, what_to_search):
        """ Iterate over the controller files and search for the given pattern
        Returns a dict with file path as the key and list of line numbers as values"""
        path_to_search = os.path.join(self.app_prefs.application_path, 'controllers')
        fn, ext = what_to_search.split('.')
        what_to_search = '{0}\.{1}'.format(fn, ext)
        pattern = re.compile(r'({fn})'.format(fn=what_to_search))
        results = {}
        for c in controllers:
            fp = os.path.join(path_to_search, c)
            try:
                with open(fp, 'r', encoding='utf-8') as f:
                    for line_num, line in enumerate(f, 1):
                        match = re.search(pattern, line)
                        if match:
                            results.setdefault(fp, []).append(line_num)
            except IOError as e:
                results.setdefault(fp, []).append(e.strerror)
        results['pattern'] = what_to_search
        return results

    def build_result_view(self, edit, results):
        """ Build a new buffer view to display the results in a navigateable form"""
        result_view = self.get_result_view()
        result_view.erase(edit, sublime.Region(0, result_view.size()))
        what_to_search = results['pattern'].replace('\\', '')
        del results['pattern']
        search_info_header = "Found reference for `{search_file}` in the following controller files \n".format(
            search_file=what_to_search)
        usage_text = '''
        # Usage:
        next line: "s"
        previous line: "w"
        show preview: "f"
        close preview: "q"
        open controller file: "enter"
        '''

        result_view.insert(edit, result_view.size(), search_info_header)
        result_regions = []
        result_file_linenum = []
        for i, k in enumerate(results.keys(), 1):
            search_info_file = '{index} - {fn}: \n'.format(index=i, fn=os.path.split(k)[-1])
            result_view.insert(edit, result_view.size(), search_info_file)
            for v in results[k]:
                search_info_func = '    Line number: {line_num}\n'.format(line_num=v)
                result_region_start = result_view.size()
                result_view.insert(edit, result_region_start, search_info_func)
                result_region_end = result_view.size()
                result_regions.append(sublime.Region(result_region_start, result_region_end))
                result_file_linenum.append((k, v))
        result_view.insert(edit, result_view.size(), usage_text)

        result_view.add_regions('result_regions', result_regions, '')
        result_regions_dict = {
            '{0},{1}'.format(region.a, region.b): result
            for region, result in zip(result_regions, result_file_linenum)}
        result_view.settings().set('result_regions_dict', result_regions_dict)
        result_view.settings().set("selected_index", -1)
        result_view.run_command("w2p_open_controller_result_navigate", {"direction": "down"})

        self.view.window().focus_view(result_view)

    def run(self, edit):
        """ Command to open the controller file"""
        app_prefs = self.app_prefs
        file_pref = split_file(self.view.file_name())
        w2p_controllers = list_controller_files()
        cont_file_name = '{cfn}{ext}'.format(cfn=file_pref.folder_name, ext='.py')
        if cont_file_name in w2p_controllers:
            cont_file = os.path.join(
                app_prefs.application_path, 'controllers', cont_file_name)
            cv = self.view.window().open_file(cont_file)
            func_reg = cv.find(file_pref.file_name, 0, sublime.LITERAL)
            cv.show_at_center(func_reg)
        else:
            results = self.search_controllers(w2p_controllers, file_pref.full_file_name)
            self.build_result_view(edit, results)
