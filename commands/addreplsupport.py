# -*- coding: utf-8 -*-
import ast
import os


import sublime
import sublime_plugin
from string import Template

from ..w2p_lib.helpers import *


class W2pAddReplSupport(sublime_plugin.WindowCommand):
    """ Command to setup a REPL for the web2py application"""

    def setup_web2py_repls(self, repl_pf):
        """Read template menu and default files

        Replace the necessary data and write out the changed template as

        SublimeREPL config

        Args:
            repl_pf(str): Path to SublimeREPL package folder
        """
        print('setup')
        app_prefs = get_application_preferences()
        cur_folder = os.path.dirname(__file__)
        package_folder, _ = os.path.split(cur_folder)
        Repl_w2p_config_folder = os.path.join(
            repl_pf,
            'config', 'web2py_{app}'.format(app=app_prefs.application_name))
        main_template_file = os.path.join(
            package_folder, 'templates', 'repl', 'Main.sublime-menu.tpl')
        def_template_file = os.path.join(
            package_folder,
            'templates', 'repl', 'Default.sublime-commands.tpl')

        with open(main_template_file, 'r') as tplfile:
            main_template = Template(tplfile.read())
        with open(def_template_file, 'r') as tplfile:
            def_template = Template(tplfile.read())
        if not os.path.exists(Repl_w2p_config_folder):
            os.makedirs(Repl_w2p_config_folder)
        main_file = os.path.join(Repl_w2p_config_folder, 'Main.sublime-menu')
        with open(main_file, 'w', encoding='utf8') as mainf:
            mainf.write(main_template.safe_substitute({
                'w2p_folder': '"{p}"'.format(
                    p=os.path.join(
                        app_prefs.web2py_folder, 'web2py.py').replace(
                        '\\', r'\\')),
                'app_name': app_prefs.application_name}))
        def_file = os.path.join(
            Repl_w2p_config_folder, 'Default.sublime-commands')
        with open(def_file, 'w', encoding='utf8') as deff:
            deff.write(def_template.safe_substitute({
                'app_name': app_prefs.application_name}))

        if sublime.ok_cancel_dialog(
                'Setup finished\nDo you want to run interactive shell now?',
                'Run shell'):
            main_map = ast.literal_eval(main_template.safe_substitute({
                'w2p_folder': '"{p}"'.format(
                    p=os.path.join(
                        app_prefs.web2py_folder, 'web2py.py').replace(
                        '\\', r'\\')),
                'app_name': app_prefs.application_name}))
            command_args = main_map[0]['children'][0]['children'][0]['args']
            self. window.run_command(
                'repl_open', command_args)

    def check_sublime_repl_install(self):
        """ Check if SublimeREPL plugin is installed

        Returns:
            path (str): if installed, empty string otherwise
        """

        repl_package_name = 'SublimeREPL'
        cur_folder = os.path.dirname(__file__)
        package_folder, _ = os.path.split(cur_folder)
        packages_folder, _ = os.path.split(package_folder)
        repl_package_folder = os.path.join(packages_folder, repl_package_name)
        if os.path.exists(repl_package_folder):
            if os.listdir(repl_package_folder):
                return repl_package_folder
            else:
                return ""
        else:
            return ""

    def run(self):
        """ Run the command"""

        error_msg = (
            'SublimeREPL packege is not installed.\n'
            'Would you like to install it now?')
        repl_package_folder = self.check_sublime_repl_install()
        if repl_package_folder:
            sublime.message_dialog('Going to install Web2py REPL support')
            self.setup_web2py_repls(repl_package_folder)
        else:
            if sublime.ok_cancel_dialog(error_msg, 'Install SublimeREPL'):
                self.window.run_command('install_package')
