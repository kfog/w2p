# -*- coding: utf-8 -*-

import sublime
import sublime_plugin

import os
import subprocess
import threading

from ..w2p_lib.helpers import get_settings, get_application_preferences


class W2pRunTests(sublime_plugin.WindowCommand):
    """ Command to run tests from specifed folder """

    def filert_test_files(self, f):
        """ Only files with `Test_` prefix are accepted """
        filename, fileext = os.path.splitext(f)
        if fileext == '.py' and filename.startswith('Test_'):
            return True
        else:
            return False

    def test_output_view(self, output):
        test_view = None
        for v in self.window.views():
            if v.name() == '*WEB2PY-TESTS*':
                test_view = v
                self.window.focus_view(v)
                # self.window.run_command('close_file')
                break
        if not test_view:
            test_view = self.window.new_file()
        test_view.set_name('*WEB2PY-TESTS*')
        test_view.set_scratch(True)
        test_view.set_read_only(True)
        self.window.focus_view(test_view)
        for l in output:
            test_view.set_read_only(False)
            test_view.run_command(
                'append',
                {'characters': '{}\n'.format(l.strip().decode('UTF-8'))})
            test_view.set_read_only(True)

    def get_tests(self):
        """ Method read all the files from the given path """

        plugin_settings = get_settings()
        app_prefs = get_application_preferences()
        test_path = plugin_settings.get('test_location', "")
        if not os.path.exists(test_path):
            tmp_folder = os.path.join(app_prefs.application_path, 'tests')
            if os.path.exists(tmp_folder):
                test_path = tmp_folder
            else:
                tmp_folder = os.path.join(
                    app_prefs.web2py_folder,
                    'tests',
                    app_prefs.application_name)
                if os.path.exists(tmp_folder):
                    test_path = tmp_folder
                else:
                    test_path = ''
                    sublime.error_message((
                        'Cannot find any tests\n'
                        'Please configure a location in settings\n'
                        'or create tests folder in application folder\n'
                        'or create tests/app_name folder in web2py folder\n'
                        'and copy tests into that folder'))
        return [
            os.path.join(test_path, f)
            for f in os.listdir(test_path) if self.filert_test_files(f)]

    def run_test(self, test_index):
        """ Run the selected test within web2py environtment

        Args:
            test_index (int): Selected test's index in the tests list"""

        print(test_index)
        app_prefs = get_application_preferences()
        tests = self.get_tests()
        if test_index == -1:
            sublime.message_dialog('Cancelled')
            return
        elif test_index == len(tests):
            for t in tests:
                command = [
                    'python',
                    '-u',
                    os.path.join(app_prefs.web2py_folder, 'web2py.py'),
                    '-S',
                    app_prefs.application_name,
                    '-M',
                    '-R',
                    t]
                self.window.active_view().set_status(
                    'w2p_test', 'Running test: {t}'.format(t=t))
                self.window.run_command('exec', {'cmd': command})
        else:
            t = tests[test_index]
            command = [
                'python',
                '-u',
                os.path.join(app_prefs.web2py_folder, 'web2py.py'),
                '-S',
                app_prefs.application_name,
                '-M',
                '-R',
                t]
            self.window.active_view().set_status(
                'w2p_test', 'Running test: {t}'.format(t=t))
            self.window.run_command('exec', {'cmd': command})

    def run(self):
        tests = self.get_tests()
        self.window.show_quick_panel(
            tests,
            self.run_test)


