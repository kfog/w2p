# -*- coding: utf-8 -*-

import os
import sublime
import sublime_plugin


from ..w2p_lib.helpers import get_application_preferences, filter_python_files


class W2pAutoComplateHelper(sublime_plugin.TextCommand):
    """ Command to import the model and module file to make
    Sublime Text auto complete aware the variables in those files"""

    def filter_settings(self, f):
        """ Filter out the files which name starts with 0"""

        # print(f)
        if f.startswith('0'):
            return False
        else:
            return True

    def get_helper_files(self):
        """ Read all the file from given folders

        Returns:
            files (list): List of the model and module files
        """

        folders = ['models', 'modules']
        app_prefs = get_application_preferences()
        files = []
        for fold in folders:
            curr_dir = os.path.join(app_prefs.application_path, fold)
            files.extend([
                os.path.splitext(f)[0]
                for f in os.listdir(curr_dir)
                if filter_python_files(f)])
        files = [f for f in files if f != '__init__']
        files = [f for f in files if self.filter_settings(f)]
        return files

    def run(self, edit):
        template_string = '    from {m} import *\n'
        helpers = [
            template_string.format(m=m)
            for m in self.get_helper_files()]
        helpers.insert(0, '    from gluon import *\n')
        helpers.insert(0, 'if False:\n')
        insert_point = self.view.sel()[0].begin()
        self.view.insert(edit, insert_point, ''.join(helpers))
