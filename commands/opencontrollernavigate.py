# -*- coding: utf-8 -*-

import sublime
import sublime_plugin


class W2pOpenControllerResultNavigate(sublime_plugin.TextCommand):
    """ Navigate between result line numbers"""

    def run(self, edit, direction):
        view_settings = self.view.settings()
        result_regions = self.view.get_regions("result_regions")
        result_region_cout = len(result_regions)
        if result_region_cout <= 0:
            return
        selected_index = int(view_settings.get("selected_index", -1))

        if direction == 'up':
            selected_index -= 1
        elif direction == 'down':
            selected_index += 1
        else:
            sublime.status_message('Invalid direction')
            return

        if selected_index < 0:
            selected_index = result_region_cout - 1
        elif selected_index > result_region_cout - 1:
            selected_index = 0

        view_settings.set('selected_index', selected_index)
        selected_region = result_regions[selected_index]
        self.view.add_regions(
            'selected_region', [selected_region], 'selected', '', sublime.DRAW_SOLID_UNDERLINE | sublime.DRAW_NO_FILL)
        self.view.show(selected_region)
