# -*- coding: utf-8 -*-

import subprocess

import sublime
import sublime_plugin

from ..w2p_lib.helpers import get_w2p_server_view


class W2pStopServer(sublime_plugin.WindowCommand):
    """ Command to stop the running web2py development server """

    def run(self):
        view = get_w2p_server_view()
        pattern = r'\"(.*?)\"'
        stop_string = view.substr(view.find(pattern, 0)).strip('"')
        stop_command = stop_string.split()
        stop_command = ['-9' if c == '-SIGTERM' else c for c in stop_command]
        proc = subprocess.Popen(
            stop_command,
            shell=False,
            stdout=subprocess.PIPE,
            stderr=subprocess.PIPE)
        proc.communicate()
        self.window.focus_view(view)
        self.window.run_command('close_file')
