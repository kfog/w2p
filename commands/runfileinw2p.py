# -*- coding: utf-8 -*-

import os
import pickle
import sublime
import sublime_plugin


from ..w2p_lib.helpers import get_application_preferences


class W2pRunFile(sublime_plugin.WindowCommand):

    def load_previosly_run_files(self):
        """ Load list of previously ran files from a pickle. It return the last 5 elements """
        pkg_dir = os.path.dirname(os.path.realpath(__file__))[:-8]
        pickle_file = os.path.join(pkg_dir, 'ran_files.pkl')
        try:
            ran_files = pickle.load(open(pickle_file, 'rb'))
        except Exception:
            ran_files = []
        return ran_files[-5:]

    def add_file_to_ran_files(self, f):
        """ Save list of ran files into a pickle """
        pkg_dir = os.path.dirname(os.path.realpath(__file__))[:-8]
        pickle_file = os.path.join(pkg_dir, 'ran_files.pkl')
        try:
            ran_files = pickle.load(open(pickle_file, 'rb'))
        except Exception:
            ran_files = []
        ran_files.append(f)
        ran_files = list(set(ran_files))
        pickle.dump(ran_files, open(pickle_file, 'wb'))

    def run_file(self, path_to_file):
        """ Build and run command """
        app_prefs = get_application_preferences()

        if os.path.exists(path_to_file):
            self.add_file_to_ran_files(path_to_file)
            command = [
                'python', '-u',
                os.path.join(app_prefs.web2py_folder, 'web2py.py'),
                '-S',
                app_prefs.application_name,
                '-M',
                '-R',
                path_to_file]
            self.window.run_command('exec', {'cmd': command})
        else:
            sublime.error_message(
                'Cannot find the file specified\n`{}`'.format(path_to_file))

    def previous_panel(self, index):
        """ Show quick panel with previously ran files """
        ran_files = self.load_previosly_run_files()
        if index > -1:
            self.run_file(ran_files[index])
        else:
            self.input_field()

    def input_field(self):
        """ Show input field to enter new file path to run """
        self.window.show_input_panel(
            'Please enter enter the path to the file you would like to run',
            '', self.run_file, None, lambda: sublime.status_message('Canceled'))

    def run(self):
        ran_files = self.load_previosly_run_files()
        if ran_files:
            self.window.show_quick_panel(ran_files, self.previous_panel)
        else:
            self.input_field()
