# -*- coding: utf-8 -*-

import os
import re
import subprocess
import threading
import webbrowser

import sublime
import sublime_plugin

from ..w2p_lib.helpers import get_settings, get_application_preferences


class W2pStartServer(sublime_plugin.WindowCommand):
    """ Command to start web2py development server with the settings provided
    in the settings file"""

    def create_server_view(self, output, server_ip):
        """ Create a read only buffer for the server output

        Args:
            output (iobuffer): output of the subporcess handling the server"""
        localhost_pattern = re.compile(r'127\.0\.0\.1')
        server_view = None
        for v in self.window.views():
            if v.name() == '*WEB2PY-SERVER*':
                self.window.run_command('close_file')
                self.window.focus_view(v)
                break
        if not server_view:
            server_view = self.window.new_file()
        server_view.set_name('*WEB2PY-SERVER*')
        server_view.set_scratch(True)
        server_view.set_read_only(True)
        self.window.focus_view(server_view)
        for l in output:
            line = l.strip().decode('UTF-8')
            line = re.sub(localhost_pattern, server_ip, line)
            server_view.set_read_only(False)
            server_view.run_command(
                'append',
                {'characters': '{}\n'.format(line)})
            server_view.set_read_only(True)

    def check_cert_and_key(self, cert, key, w2p_folder):
        """ Check if the provided certificate and key are valid

        Args:
            cert (str): Path to cert or cert name
            key (str): Path to key or key name
            w2p_folder (str): Path to web2py
        Returns:
            error (str): Error message if not valid empty otherwise"""

        error = ""
        if cert:
            if not os.path.exists(cert):
                cert = os.path.join(w2p_folder, cert)
                if not os.path.exists(cert):
                    error += (
                        'No valid certificate configured\n'
                        'Please copy certificate to the web2py base folder\n'
                        'or enter full path to a valid certificate '
                        'in settings')
        if key:
            if not os.path.exists(key):
                key = os.path.join(w2p_folder)
                if not os.path.join(key):
                    error += (
                        'No valid key configured\n'
                        'Please copy key to the web2py base folder\n'
                        'or enter full path to a valid key in settings')
        return error

    def build_start_command(self):
        """ Build a list of start server command components

        Returns:
            command_base (list)"""

        plugin_settings = get_settings()
        app_prefs = get_application_preferences()

        key = plugin_settings.get('server_key', '')
        cert = plugin_settings.get('server_cert', '')
        error = self.check_cert_and_key(cert, key, app_prefs.web2py_folder)
        if error:
            return error

        w2p_file = os.path.join(app_prefs.web2py_folder, 'web2py.py')
        command_base = ['python', '-u', w2p_file]
        ip_address = plugin_settings.get('server_ip', '127.0.0.1')
        port = plugin_settings.get('server_port', '8080')

        passwd = plugin_settings.get('server_password', 'Passw0rd')
        parameters = [
            '-i',
            ip_address,
            '-p',
            port,
            '-a',
            passwd,
            '-c',
            cert,
            '-k',
            key]
        command_base.extend(parameters)
        return command_base

    def open_in_browser(self, command, appname):
        """ Open the app in a new browser tab

        Args:
            command (list): The start command
            appname (str): Name of the application"""

        plugin_settings = get_settings()
        oib = plugin_settings.get('open_in_browser', True)
        if oib:
            print('Opening in new tab')
            base_url = '{prot}://{host}:{port}/{app}'
            if command[8]:
                prot = 'https'
            else:
                prot = 'http'
            base_url = base_url.format(
                    prot=prot,
                    host='localhost',
                    port=command[4],
                    app=appname)
            webbrowser.open_new_tab(base_url)

    def run(self):
        command = self.build_start_command()
        if isinstance(command, list):
            proc = subprocess.Popen(
                command,
                shell=False,
                stdout=subprocess.PIPE,
                stderr=subprocess.PIPE)
            t = threading.Thread(
                target=self.create_server_view,
                args=[proc.stdout, command[4]])
            t.start()
            _, app_name = os.path.split(self.window.folders()[0])
            self.open_in_browser(command, app_name)
        else:
            sublime.error_message(command)
