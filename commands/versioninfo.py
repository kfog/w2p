# -*- coding: utf-8 -*-

import sublime
import sublime_plugin

from ..w2p_lib.version import get_version


class W2pShowVersionInfo(sublime_plugin.WindowCommand):

    def run(self):
        header = 'W2P - Web2py plugin for Sublime Text 3'
        contact = 'Contact information: gergelyo@gmail.com'
        ver = 'Version: {}'.format(".".join([str(v) for v in get_version()]))
        info = [header, contact, ver]
        sublime.message_dialog("\n".join(info))
