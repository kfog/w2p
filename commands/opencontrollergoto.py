# -*- coding: utf-8 -*-

import sublime
import sublime_plugin


class W2pOpenControllerGoto(sublime_plugin.TextCommand):
    """ Open the selected controller file and centers on the pattern"""

    def run(self, edit, preview=False):
        view_settings = self.view.settings()
        result_regions = self.view.get_regions("result_regions")
        result_region_cout = len(result_regions)

        if result_region_cout <= 0:
            return

        selected_index = int(view_settings.get("selected_index", -1))
        if selected_index < 0 or selected_index > result_region_cout - 1:
            sublime.status_message("Open Controller: Select a controller first!")
            return

        selected_region = result_regions[selected_index]
        result_regions_dict = self.view.settings().get('result_regions_dict')
        result = result_regions_dict.get('{0},{1}'.format(selected_region.a, selected_region.b))
        if not result:
            sublime.status_message('Cannot find corresponding file and function')
            return

        active_window = self.view.window()
        filepath, linenum = result
        if preview:
            active_window.run_command("set_layout", {
                "cols": [0.0, 1.0],
                "rows": [0.0, 0.5, 1.0],
                "cells": [[0, 0, 1, 1], [0, 1, 1, 2]]})

            active_window.focus_group(1)
            new_view = active_window.open_file(
                "{filepath}:{linenum}".format(filepath=filepath, linenum=linenum),
                sublime.ENCODED_POSITION | sublime.TRANSIENT)
            new_view.settings().set("open_controller_preview_window", True)
            active_window.focus_view(self.view)
        else:
            self.view.window().run_command("set_layout", {
                "cols": [0.0, 1.0],
                "rows": [0.0, 1.0],
                "cells": [[0, 0, 1, 1]]})
            new_view = active_window.open_file("{filepath}:{linenum}".format(
                filepath=filepath, linenum=linenum), sublime.ENCODED_POSITION)
