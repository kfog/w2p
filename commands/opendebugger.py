# -*- coding: utf-8 -*-

import sublime
import sublime_plugin
import webbrowser
import urllib
import threading


from ..w2p_lib.helpers import get_settings


class W2pOpenDebugger(sublime_plugin.WindowCommand):

    def check_we2py_is_runing(self, url):
        choise = 0
        msg = 'Web2py development server is not running.\nWould you like to start it now?'
        yes_msg = 'Start server now'
        no_msg = 'Not now'
        try:
            urllib.request.urlretrieve(url)
            webbrowser.open_new_tab(url)
        except urllib.error.HTTPError:
            choise = sublime.yes_no_cancel_dialog(msg, yes_msg, no_msg)
        except urllib.error.URLError:
            choise = sublime.yes_no_cancel_dialog(msg, yes_msg, no_msg)

        if choise == 1:
            self.window.run_command('w2p_start_server')
            return
        else:
            return

    def run(self):
        plugin_settings = get_settings()
        port = plugin_settings.get('server_port', '8080')
        url = 'https://localhost:{p}/admin/debug/interact'.format(p=port)
        check_thread = CheckW2pThread(self.check_we2py_is_runing, url)
        check_thread.start()


class CheckW2pThread(threading.Thread):

    def __init__(self, check_function, url):
        self.check_function = check_function
        self.url = url
        threading.Thread.__init__(self)

    def run(self):
        self.check_function(self.url)
