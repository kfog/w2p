# -*- coding: utf8 -*-

import sublime
import sublime_plugin

from ..w2p_lib.helpers import get_settings


class W2pOnPostSaveEventListener(sublime_plugin.EventListener):

    def on_post_save_async(self, view):
        """ If a module is edited and saved the server will be restarted """
        if 'modules' in view.file_name():
            plugin_settings = get_settings()
            if plugin_settings.get('restart_on_module_change', True):
                sublime.active_window().run_command('w2p_restart_server')

