# -*- coding: utf8 -*-

import sublime_plugin
import subprocess


class W2pOnCloseEventListener(sublime_plugin.EventListener):

    def on_close(self, view):
        """ on_close event. If the closed view is the server output view
        The server process will be terminated """
        pattern = r'\"(.*?)\"'
        if view.name() == '*WEB2PY-SERVER*':
            stop_string = view.substr(view.find(pattern, 0)).strip('"')
            stop_command = stop_string.split()
            if stop_command:
                print('Stoping web2py server with pid: {}'.format(
                    stop_command[-1]))
                subprocess.Popen(
                    stop_command,
                    shell=True)
            else:
                return
