New functionality:
    Open controller file
    It will open the corresponding controller file.
    If generic view is selected all the controller files will be searched and the sumer will be display on a new file
    You can navigate between appearances, preview and open them

Settings:
    Added `restart_on_module_change` key with default value `false` to the setting file.
    It prevents to restart the development server when a module file is changed
